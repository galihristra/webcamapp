﻿using DirectX.Capture;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace WebcamApps
{
    public partial class MainForm : Form
    {
        // Initialization
        private Capture capture = null;
        private Filters filters = new Filters();
        bool isRecording = false;
        private static int recordDuration = 15; // Set Record Duration Here
        private static int counter = recordDuration;

        public MainForm()
        {
            this.InitializeComponent();
            this.timerLabel.Text = "Timer : " + recordDuration.ToString();

            this.capture = new Capture(this.filters.VideoInputDevices[0], this.filters.AudioInputDevices[0]);
            //capture.CaptureComplete += new EventHandler(OnCaptureComplete);

            // Compression Settings
            //foreach (Filter item in this.filters.VideoCompressors)
            //{
            //    MessageBox.Show(item.Name);
            //}
            //Filter videoCodec = (from Filter f in this.filters.VideoCompressors
            //                     where f.Name == "DV Video Encoder"
            //                     select f).FirstOrDefault();

            //this.capture.VideoCompressor = videoCodec;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.blinkTimer.Start();
        }

        private void StartRecording()
        {
            this.capture.PreviewWindow = this.videoBox;
            this.capture.Filename = this.FileNameGenerator();

            if (!this.capture.Cued)
            {
                this.capture.Cue();
            }
            this.capture.Start();

            this.recordTimer.Enabled = true;
            this.recordTimer.Start();
            this.isRecording = true;

            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.startRecordBtn.Text = "Stop";
                    this.nameBox.Enabled = false;
                }));
            }
            else
            {
                this.startRecordBtn.Text = "Stop";
                this.nameBox.Enabled = false;
            }
        }

        private void StopRecording()
        {
            this.capture.Stop();
            this.recordTimer.Stop();
            this.isRecording = false;

            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.startRecordBtn.Text = "Start Record";
                    this.nameBox.Enabled = true;
                    this.capture.PreviewWindow = null;
                    this.timerLabel.Text = "Timer : " + recordDuration.ToString();
                }));
            }
            else
            {
                this.startRecordBtn.Text = "Start Record";
                this.nameBox.Enabled = true;
                this.capture.PreviewWindow = null;
                this.timerLabel.Text = "Timer : " + recordDuration.ToString();
            }

            MessageBox.Show("Recording Done !");
        }

        private void StartRecordButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.isRecording)
                {
                    this.StopRecording();
                }
                else
                {
                    if (string.IsNullOrEmpty(this.nameBox.Text))
                    {
                        MessageBox.Show("Please Enter Your Name First.");
                    }
                    else
                    {
                        if (this.capture == null)
                        {
                            throw new ApplicationException("Invalid Video/Audio Input Device.");
                        }

                        // setting up timer
                        this.recordTimer.Interval = 1000; // 1 second interval
                        this.recordTimer.Tick += new EventHandler(this.OnTimerTick);

                        this.StartRecording();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\n" + ex.ToString());
            }
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            TimeSpan timespan = new TimeSpan();
            if (counter > 1)
            {
                counter = counter - 1;
                timespan = TimeSpan.FromSeconds(counter);
                this.timerLabel.Text = "Timer : " + timespan.ToString(@"ss");
            }
            else
            {
                counter = counter - 1;
                timespan = TimeSpan.FromSeconds(counter);
                this.timerLabel.Text = "Timer : " + timespan.ToString(@"ss");

                this.StopRecording();
                this.recordTimer.Stop();
                this.recordTimer.Tick -= new EventHandler(this.OnTimerTick);
                this.recordTimer.Enabled = false;
                this.recordTimer.Dispose();

                counter = recordDuration;
                timespan = TimeSpan.FromSeconds(counter);
                this.timerLabel.Text = "Timer : " + recordDuration.ToString();
            }
        }

        private string FileNameGenerator()
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\Videos\\"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Videos\\");
            }

            DateTime date = DateTime.Now;
            string filePath = Directory.GetCurrentDirectory() + "\\Videos\\" + string.Format("\\{0}-{1}-{2}-{3} {4}-{5}-{6}.avi",
                this.nameBox.Text,
                date.Year,
                date.Month,
                date.Day,
                date.Hour,
                date.Minute,
                date.Second);

            return filePath;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.isRecording)
            {
                this.StopRecording();
            }
        }

        private void nameBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetterOrDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void openRecordFolderBtn_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\Videos\\"))
            {
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Videos\\");
            }

            Process.Start("explorer.exe", Directory.GetCurrentDirectory() + "\\Videos\\");
        }

        private void blinkTimer_Tick(object sender, EventArgs e)
        {
            if (this.blinkLabel.Visible)
            {
                this.blinkLabel.Visible = false;
            }
            else
            {
                this.blinkLabel.Visible = true;
            }
        }
    }
}
