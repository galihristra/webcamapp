﻿namespace WebcamApps
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToggleButton = new System.Windows.Forms.Button();
            this.CameraList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.durationText = new System.Windows.Forms.NumericUpDown();
            this.startRecordBtn = new System.Windows.Forms.Button();
            this.resolutionList = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.videoBox = new System.Windows.Forms.PictureBox();
            this.blinkLabel = new System.Windows.Forms.Label();
            this.openRecordFolderBtn = new System.Windows.Forms.Button();
            this.timerLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.recordTimer = new System.Windows.Forms.Timer(this.components);
            this.blinkTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.durationText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ToggleButton
            // 
            this.ToggleButton.Location = new System.Drawing.Point(611, 587);
            this.ToggleButton.Name = "ToggleButton";
            this.ToggleButton.Size = new System.Drawing.Size(211, 63);
            this.ToggleButton.TabIndex = 1;
            this.ToggleButton.Text = "Start Webcam Test";
            this.ToggleButton.UseVisualStyleBackColor = true;
            this.ToggleButton.Visible = false;
            // 
            // CameraList
            // 
            this.CameraList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CameraList.FormattingEnabled = true;
            this.CameraList.Location = new System.Drawing.Point(659, 30);
            this.CameraList.Name = "CameraList";
            this.CameraList.Size = new System.Drawing.Size(211, 21);
            this.CameraList.TabIndex = 2;
            this.CameraList.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(656, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select Video Source";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(179, 531);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Duration (in seconds)";
            this.label2.Visible = false;
            // 
            // durationText
            // 
            this.durationText.Location = new System.Drawing.Point(289, 529);
            this.durationText.Name = "durationText";
            this.durationText.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.durationText.Size = new System.Drawing.Size(101, 20);
            this.durationText.TabIndex = 6;
            this.durationText.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.durationText.Visible = false;
            // 
            // startRecordBtn
            // 
            this.startRecordBtn.ImageKey = "(none)";
            this.startRecordBtn.Location = new System.Drawing.Point(647, 73);
            this.startRecordBtn.Name = "startRecordBtn";
            this.startRecordBtn.Size = new System.Drawing.Size(162, 56);
            this.startRecordBtn.TabIndex = 7;
            this.startRecordBtn.Text = "Start Record !";
            this.startRecordBtn.UseVisualStyleBackColor = true;
            this.startRecordBtn.Click += new System.EventHandler(this.StartRecordButton_Click);
            // 
            // resolutionList
            // 
            this.resolutionList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.resolutionList.FormattingEnabled = true;
            this.resolutionList.Location = new System.Drawing.Point(659, 95);
            this.resolutionList.Name = "resolutionList";
            this.resolutionList.Size = new System.Drawing.Size(211, 21);
            this.resolutionList.TabIndex = 8;
            this.resolutionList.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(656, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Select Resolution";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label3.Visible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.videoBox);
            this.splitContainer1.Panel1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.blinkLabel);
            this.splitContainer1.Panel2.Controls.Add(this.openRecordFolderBtn);
            this.splitContainer1.Panel2.Controls.Add(this.timerLabel);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.nameBox);
            this.splitContainer1.Panel2.Controls.Add(this.startRecordBtn);
            this.splitContainer1.Size = new System.Drawing.Size(979, 702);
            this.splitContainer1.SplitterDistance = 498;
            this.splitContainer1.TabIndex = 11;
            this.splitContainer1.TabStop = false;
            // 
            // videoBox
            // 
            this.videoBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.videoBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.videoBox.Location = new System.Drawing.Point(169, 9);
            this.videoBox.MaximumSize = new System.Drawing.Size(640, 480);
            this.videoBox.Name = "videoBox";
            this.videoBox.Size = new System.Drawing.Size(640, 480);
            this.videoBox.TabIndex = 0;
            this.videoBox.TabStop = false;
            // 
            // blinkLabel
            // 
            this.blinkLabel.AutoSize = true;
            this.blinkLabel.ForeColor = System.Drawing.Color.Red;
            this.blinkLabel.Location = new System.Drawing.Point(167, 135);
            this.blinkLabel.Name = "blinkLabel";
            this.blinkLabel.Size = new System.Drawing.Size(251, 13);
            this.blinkLabel.TabIndex = 13;
            this.blinkLabel.Text = "*Video Ini Hanya Akan Merekam Maksimal 15 Detik";
            // 
            // openRecordFolderBtn
            // 
            this.openRecordFolderBtn.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.openRecordFolderBtn.Location = new System.Drawing.Point(646, 135);
            this.openRecordFolderBtn.Name = "openRecordFolderBtn";
            this.openRecordFolderBtn.Size = new System.Drawing.Size(162, 29);
            this.openRecordFolderBtn.TabIndex = 12;
            this.openRecordFolderBtn.Text = "Open Recording Folder";
            this.openRecordFolderBtn.UseVisualStyleBackColor = true;
            this.openRecordFolderBtn.Visible = false;
            this.openRecordFolderBtn.Click += new System.EventHandler(this.openRecordFolderBtn_Click);
            // 
            // timerLabel
            // 
            this.timerLabel.AutoSize = true;
            this.timerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timerLabel.Location = new System.Drawing.Point(672, 43);
            this.timerLabel.Name = "timerLabel";
            this.timerLabel.Size = new System.Drawing.Size(109, 26);
            this.timerLabel.TabIndex = 11;
            this.timerLabel.Text = "Timer : 15";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(165, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(194, 26);
            this.label4.TabIndex = 9;
            this.label4.Text = "Enter Your Name :";
            // 
            // nameBox
            // 
            this.nameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameBox.Location = new System.Drawing.Point(169, 73);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(472, 56);
            this.nameBox.TabIndex = 8;
            this.nameBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nameBox_KeyPress);
            // 
            // recordTimer
            // 
            this.recordTimer.Interval = 1000;
            // 
            // blinkTimer
            // 
            this.blinkTimer.Interval = 1000;
            this.blinkTimer.Tick += new System.EventHandler(this.blinkTimer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1004, 726);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.resolutionList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CameraList);
            this.Controls.Add(this.ToggleButton);
            this.Controls.Add(this.durationText);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Webcam App";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.durationText)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ToggleButton;
        private System.Windows.Forms.ComboBox CameraList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown durationText;
        private System.Windows.Forms.Button startRecordBtn;
        private System.Windows.Forms.ComboBox resolutionList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox videoBox;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer recordTimer;
        private System.Windows.Forms.Label timerLabel;
        private System.Windows.Forms.Button openRecordFolderBtn;
        private System.Windows.Forms.Label blinkLabel;
        private System.Windows.Forms.Timer blinkTimer;
    }
}

